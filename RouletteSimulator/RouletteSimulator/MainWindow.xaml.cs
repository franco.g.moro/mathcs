﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RouletteSimulator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Dictionary<Button,List<int>> hash;
        List<int> Blacks = new List<int>();
        List<int> Reds = new List<int>();
        List<Button> NewBet = new List<Button>();
        List<List<int>> CurrentBet = new List<List<int>>();
        List<Button> DisabledButtons = new List<Button>();

        public MainWindow()
        {
            InitializeComponent();
            CreateAllSockets();
            hash = new Dictionary<Button, List<int>>();
            PlaceAllButtons();
        }
        /// <summary>
        /// The Parent method of all the methods placing the buttons on the roulette board
        /// </summary>
        private void PlaceAllButtons()
        {
            PlaceInnerButtons();
            PlaceStreets();
            PlaceBlock12Buttons();
            PlaceColumnGridButtons();
            PlaceHalfBoardGridButton();
            Game.Background = new SolidColorBrush(Colors.Green);
        }
        /// <summary>
        /// This method creates all the "Sockets" to hold the buttons
        /// </summary>
        private void CreateAllSockets()
        {
            CreateInsideBoardSockets();
            CreateStreetGridSockets();
            CreateBlock12GridSockets();
            CreateHalfBoardSockets();
            CreateColumnGridSockets();

        }
        /// <summary>
        /// Places all the street type buttons.
        /// </summary>
        private void PlaceStreets()
        {
            int from =1;
            int to = 3;

            for(int i = 0; i < 12; i++)
            {
                Button btn = CreateButton(from,to);
                StreetGrid.Children.Add(btn);
                Grid.SetColumn(btn, i);
                Grid.SetRow(btn,0);
                from += 3;
                to += 3;
            }

        }
        /// <summary>
        /// Places all the Row buttons (which englobe 12 numbers).
        /// </summary>
        private void PlaceBlock12Buttons()
        {
            int from = 1;
            int to = 12;
            for(int i = 0; i < 3; i++)
            {
                Button btn = CreateButton(from, to);
                Block12Grid.Children.Add(btn);
                Grid.SetColumn(btn, i);
                Grid.SetRow(btn, 0);
                from += 12;
                to += 12;
            }
        }
        /// <summary>
        /// Creates the socket for the number buttons also known as inside bets.
        /// </summary>
        private void CreateInsideBoardSockets()
        {
            InsideBoard.Children.Clear();
            InsideBoard.RowDefinitions.Clear();
            InsideBoard.ColumnDefinitions.Clear();
            for (int i = 0; i < 3; i++)
            {
                RowDefinition row = new RowDefinition();
                InsideBoard.RowDefinitions.Add(row);
            }
            for (int i = 0; i < 12; i++)
            {
                ColumnDefinition col = new ColumnDefinition();
                InsideBoard.ColumnDefinitions.Add(col);
            }
        }
        /// <summary>
        /// Creates the "Sockets" the Street type buttons.
        /// </summary>
        private void CreateStreetGridSockets()
        {
            RowDefinition row = new RowDefinition();
            StreetGrid.RowDefinitions.Add(row);
            for(int i = 0; i < 12; i++)
            {
                ColumnDefinition col = new ColumnDefinition();
                StreetGrid.ColumnDefinitions.Add(col);
            }
        }
        /// <summary>
        /// Creates the "Sockets" the 1/3 of board type buttons.
        /// </summary>
        private void CreateBlock12GridSockets()
        {
            RowDefinition row = new RowDefinition();
            Block12Grid.RowDefinitions.Add(row);
            for (int i = 0; i < 3; i++)
            {
                ColumnDefinition col = new ColumnDefinition();
                Block12Grid.ColumnDefinitions.Add(col);
            }
        }
        /// <summary>
        /// Creates the "Sockets" the 1/2 board type buttons.
        /// </summary>
        private void CreateHalfBoardSockets()
        {
            RowDefinition row = new RowDefinition();
            HalfBoardGrid.RowDefinitions.Add(row);
            for (int i = 0; i < 6; i++)
            {
                ColumnDefinition col = new ColumnDefinition();
                HalfBoardGrid.ColumnDefinitions.Add(col);
            }
        }
        /// <summary>
        /// Creates the "Sockets" the Row type buttons.
        /// </summary>
        private void CreateColumnGridSockets()
        {
            ColumnDefinition col = new ColumnDefinition();
            ColumnGrid.ColumnDefinitions.Add(col);
            for(int i = 0; i < 3; i++)
            {
                RowDefinition row = new RowDefinition();
                ColumnGrid.RowDefinitions.Add(row);
            }
        }
        /// <summary>
        /// Places the buttons for inner bets.
        /// </summary>
        private void PlaceInnerButtons()
        {
            int pos = 1;
            for( int i = 0; i < 12; i++)
            {
                for(int j =2; j >=0; j--)
                {
                    Button btn = CreateButton(pos);
                    InsideBoard.Children.Add(btn);
                    Grid.SetColumn(btn, i);
                    Grid.SetRow(btn, j);
                    pos++;

                }
            }
        }
        /// <summary>
        /// Places the buttons on the right side (rows if 12).
        /// </summary>
        private void PlaceColumnGridButtons()
        {
            int from = 3;
            int to = 36;
            for(int i = 0; i < 3; i++)
            {
                Button btn = CreateButton(from, to);
                ColumnGrid.Children.Add(btn);
                Grid.SetColumn(btn, 0);
                Grid.SetRow(btn, i);
                from--;
                to--;
            }
        }
        /// <summary>
        /// Places the buttons englobing half the board.
        /// </summary>
        private void PlaceHalfBoardGridButton()
        {
            List<Button> buttons = new List<Button>();
            buttons.Add(CreateButton(1, 18));
            buttons.Add(CreateButton(2, 2));
            buttons.Add(CreateButton(0, 1));
            buttons.Add(CreateButton(1, 0));
            buttons.Add(CreateButton(1, 1));
            buttons.Add(CreateButton(19, 36));
            int pos = 0;
            foreach(Button x in buttons)
            {
                HalfBoardGrid.Children.Add(x);
                Grid.SetColumn(x, pos);
                Grid.SetRow(x, 0);
                pos++;
            }
;        }
        /// <summary>
        /// Method that creates the inner Buttons in the board.
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        private Button CreateButton(int n)
        {
            Button btn = new Button();
            btn.Click += btnClick;
            btn.Content = n.ToString();
            btn.Foreground = new SolidColorBrush(Colors.White);
            btn.BorderBrush = new SolidColorBrush(Colors.White);
            if (n == 28||n==10)
            {
                btn.Background = new SolidColorBrush(Colors.Black);
                Blacks.Add(n);
            }
            else if ((n > 18 && n < 28)||(n < 10))
            {
                if (n % 2 == 1)
                {
                    btn.Background = new SolidColorBrush(Colors.Red);
                    Reds.Add(n);
                }
                else
                {
                    btn.Background = new SolidColorBrush(Colors.Black);
                    Blacks.Add(n);
                }
            }
            else
            {
                if (n % 2 == 0)
                {
                    btn.Background = new SolidColorBrush(Colors.Red);
                    Reds.Add(n);
                }
                else
                {
                    btn.Background = new SolidColorBrush(Colors.Black);
                    Blacks.Add(n);
                }
            }
            List<int> list = new List<int>();
            list.Add(n);
            hash.Add(btn,list);
            return btn;
        }
        /// <summary>
        /// Create all buttons of any other type than inner bet ones.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        private Button CreateButton(int from,int to)
        {
            Button btn = new Button();
            btn.Click += btnClick;
            List<int> list = new List<int>();
            int number = from;
            btn.Foreground = new SolidColorBrush(Colors.White);
            btn.BorderBrush = new SolidColorBrush(Colors.White);
            btn.Background = new SolidColorBrush(Colors.Green);
            if ((from + 2) == to)
            {
                btn.Content = "Street";
                while (number <= to)
                {
                    list.Add(number);
                    number++;
                }
            }
            else if ((from+33)==to ){
                btn.Content = "2:1";
                while (number <= to)
                {
                    list.Add(number);
                    number+=3;
                }
            }
            else if ((from + 17) == to)
            {
                while (number <= to)
                {
                    list.Add(number);
                    number++;
                }
                if (from == 1)
                {
                    btn.Content = "1 to 18";
                }
                else
                {
                    btn.Content = "19 to 36";
                }
            }
            else if ((from + 11) == to)
            {
                if (from == 1)
                {
                    btn.Content = "1st 12";
                    while (number <= to)
                    {
                        list.Add(number);
                        number++;
                    }
                }
                else if (from == 13)
                {
                    btn.Content = "2nd 12";
                    while (number <= to)
                    {
                        list.Add(number);
                        number++;
                    }
                }
                else
                {
                    btn.Content = "3rd 12";
                    while (number <= to)
                    {
                        list.Add(number);
                        number++;
                    }
                }
            }
            else if (from==2&& to == 2)
            {
                btn.Content = "Even";
                for(int i = 1; i < 37; i++)
                {
                    if (i % 2 == 0)
                    {
                        list.Add(i);
                    }
                }
            }
            else if (from == 1 && to == 1)
            {
                btn.Content = "Odd";
                for (int i = 1; i < 37; i++)
                {
                    if (!(i % 2 == 0))
                    {
                        list.Add(i);
                    }
                }
            }
            else
            {
                if (from == 0)
                {
                    btn.Content = "Black";
                    btn.Background = new SolidColorBrush(Colors.Black);
                    list = Blacks;
                }
                else
                {
                    btn.Content = "Red";
                    btn.Background = new SolidColorBrush(Colors.Red);
                    list = Reds;
                }
            }
            hash.Add(btn,list);
            return btn;
        }
        /// <summary>
        /// Event handler for buttons clicks.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClick(object sender, RoutedEventArgs e)
        {
            Button btn=(Button)sender;
            SolidColorBrush color = new SolidColorBrush(Colors.Blue);
            btn.BorderBrush = color;
            btn.Foreground = color;
            if (!(NewBet.Contains(btn)))
            {
                NewBet.Add(btn);
            }
        }
        /// <summary>
        /// Event handler for the AddBetButton.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddBet_Click(object sender, RoutedEventArgs e)
        {
            if (NewBet.Count == 0)
            {
                MessageBox.Show("No bet Entered");
            }
            if (NewBet.Count() > 4)
            {
                MessageBox.Show("Too many Arguments");
                SolidColorBrush color = new SolidColorBrush(Colors.White);
                foreach (Button x in NewBet)
                {
                    x.BorderBrush = color;
                    x.Foreground = color;
                }
                NewBet.Clear();
            }
            else
            {
                List<List<int>> Checker = new List<List<int>>();
                List<int> value=new List<int>();
                foreach (Button btn in NewBet)
                {
                    if (hash.TryGetValue(btn, out value))
                    {
                        Checker.Add(value);
                    }
                }
                if (Checker.Count == 3)
                {
                    MessageBox.Show("Invalid Bet");
                    SolidColorBrush color = new SolidColorBrush(Colors.White);
                    foreach (Button x in NewBet)
                    {
                        x.BorderBrush = color;
                        x.Foreground = color;
                    }
                }
                //Single Number inner bet
                else if (Checker.Count() == 1&&Checker[0].Count()==1)
                {
                    CurrentBet.Add(Checker[0]);
                    foreach (Button x in NewBet)
                    {
                        x.IsEnabled = false;
                        DisabledButtons.Add(x);
                    }
                }
                //Square Bet
                else if (Checker.Count()==4 && Checker[0].Count() == 1&&Checker[1].Count() == 1&&Checker[2].Count() == 1&&Checker[3].Count()==1)
                {
                    List<int> Square = new List<int>();
                    foreach(List<int> x in Checker)
                    {
                        Square.Add(x[0]);
                    }
                    Square.Sort();
                    if (Square[0] + 1 == Square[1] && Square[0] + 3 == Square[2] && Square[0] + 4 == Square[3])
                    {
                        CurrentBet.Add(Square);
                        foreach (Button x in NewBet)
                        {
                            x.IsEnabled = false;
                            DisabledButtons.Add(x);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Invalid Bet");
                        SolidColorBrush color = new SolidColorBrush(Colors.White);
                        foreach (Button x in NewBet)
                        {
                            x.BorderBrush = color;
                            x.Foreground = color;
                        }
                    }
                }
                //Split
                else if (Checker.Count() == 2&&Checker[0].Count()==1&&Checker[1].Count()==1)
                {
                    List<int> split = new List<int>();
                    foreach (List<int> x in Checker)
                    {
                            split.Add(x[0]);
                    }
                    split.Sort();
                    if (split.Count() == 2)
                    {
                        if (split[0] + 1 == split[1] || split[0] + 3 == split[1])
                        {
                            CurrentBet.Add(split);
                            foreach (Button x in NewBet)
                            {
                                x.IsEnabled = false;
                                DisabledButtons.Add(x);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Invalid Bet");
                            SolidColorBrush color = new SolidColorBrush(Colors.White);
                            foreach (Button x in NewBet)
                            {
                                x.BorderBrush = color;
                                x.Foreground = color;
                            }
                            NewBet.Clear();
                        }
                    }
                }
                //Outside Bets
                else if (Checker.Count()==1&&Checker[0].Count>1)
                {
                    CurrentBet.Add(Checker[0]);
                    foreach (Button x in NewBet)
                    {
                        x.IsEnabled = false;
                        DisabledButtons.Add(x);
                    }
                }
                else
                {
                    MessageBox.Show("Invalid Bet");
                    SolidColorBrush color = new SolidColorBrush(Colors.White);
                    foreach (Button x in NewBet)
                    {
                        x.BorderBrush = color;
                        x.Foreground = color;
                    }
                }
                NewBet.Clear();
                BetBoxDisplay();
            }
        }
        /// <summary>
        /// Even handler for the reset button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Reset_Click(object sender, RoutedEventArgs e)
        {
            SolidColorBrush color = new SolidColorBrush(Colors.White);
            foreach (Button x in DisabledButtons)
            {
                x.IsEnabled=true;
                x.BorderBrush = color;
                x.Foreground = color;
            }
            foreach(Button x in NewBet)
            {
                x.BorderBrush = color;
                x.Foreground = color;
            }
            NewBet.Clear();
            DisabledButtons.Clear();
            CurrentBet.Clear();
            BetBoxDisplay();
        }
        /// <summary>
        /// This method is used to display what integers are included in the current bet.
        /// </summary>
        private void BetBoxDisplay()
        {
            BetBox.Children.Clear();
            foreach(List<int> x in CurrentBet)
            {
                TextBlock bet = new TextBlock();
                string BetContent = "";
                if (x.Count() == 18)
                {
                    BetContent = "HalfBoard, 1:1 {";
                }
                else if (x.Count() == 12)
                {
                    BetContent = "Dozen/Column, 1:2 {";
                }
                else if (x.Count() == 1)
                {
                    BetContent = "Straight Up, 1:35 {";
                }
                else if (x.Count() == 2)
                {
                    BetContent = "Split, 1:17 {";
                }
                else if (x.Count == 4)
                {
                    BetContent = "Corner, 1:8 {";
                }
                else
                {
                    BetContent = "Street, 1:11 {";
                }
                foreach (int i in x)
                {
                    if (x.IndexOf(i) != x.Count() - 1)
                    {
                        BetContent += i.ToString() + ", ";
                    }
                    else
                    {
                        BetContent += i.ToString() + "}";
                    }
                }
                bet.Text = BetContent;
                BetBox.Children.Add(bet);
            }
        }
        /// <summary>
        /// This is the even handler for the Button Spin, it also starts the process of computation of all the fields displayed on the end screen.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Spin_Click(object sender, RoutedEventArgs e)
        {
            int nSpins=0;
            int aBet=0;
            try
            {
                nSpins = Convert.ToInt32(NSpins.Text);
                aBet = Convert.ToInt32(ABet.Text);
            }
            catch(FormatException exc)
            {
                MessageBox.Show("Please enter numbers only");
                return;
            }
            finally
            {
                List<int> Outcomes = SpinResults(nSpins);
                List<long> Oij = ComputeOutcomes(Outcomes,aBet);
                List<long> Eij = ComputeExpectedValue(Outcomes.Count(), aBet);
                long total = 0;
                foreach (long x in Oij)
                {
                    total += x;
                }
                Results_Screen.Visibility = Visibility.Visible;
                TotalGain.Text = "Total Gain: " + total.ToString();
                double mean = (total / nSpins);
                MeanGain.Text = "Mean Gain: " + mean.ToString();
                double Sd = ComputeStandardDeviation(Oij, mean);
                if (Sd == -1)
                {
                    StandardDeviation.Text = "Std. Dev :  Spin n<2";
                }
                else
                {
                    StandardDeviation.Text = "Std. Dev. : " + Sd.ToString();
                }
                SpinResultBox.Text = SpinResultPrintout(Outcomes);
                bool hypoTest = HypothesisTest(Sd, nSpins, mean);
                if (hypoTest==true)
                {
                    HoBox.Text = "Ho is accepted";
                }
                else
                {
                    HoBox.Text = "Ho is rejected";
                }
                FalseTrue.Text = "The claim is: " + hypoTest.ToString();


            }

        }
        /// <summary>
        /// This method is in charge of displaying what is on the last panel after a spin.
        /// </summary>
        /// <param name="outcomes"></param>
        /// <returns></returns>
        private string SpinResultPrintout(List<int> outcomes)
        {
            string output = "Spin results: {";
            for(int x=0;x<outcomes.Count();x++)
            {
                if (!(x == outcomes.Count() - 1))
                {
                    output += outcomes[x].ToString() + ", ";
                }
                else
                {
                    output+= outcomes[x].ToString()+ "}";
                }
            }
            return output;
        }
        /// <summary>
        /// This methods simulates the roulette spin results.
        /// </summary>
        /// <param name="nSpins"></param>
        /// <returns></returns>
        private List<int> SpinResults(int nSpins)
        {
            Random rdm = new Random();
            List<int> Output = new List<int>();
            for(int i = 0; i < nSpins; i++)
            {
              Output.Add(rdm.Next(37));
            }
            return Output;
        }
        /// <summary>
        /// Computes the standard deviation of the gain per spin of the roulette.
        /// </summary>
        /// <param name="Oij"></param>
        /// <param name="mean"></param>
        /// <returns></returns>
        private double ComputeStandardDeviation(List<long> Oij, double mean)
        {
            if (Oij.Count == 1)
            {
                return -1;
            }
            double sd = 0;
            foreach( long x in Oij)
            {
                sd += (x - mean) * (x - mean);
            }
            sd = sd / (Oij.Count()-1);
            sd =Math.Sqrt(sd);
            sd = Math.Round(sd,3);
            return sd;
        }
        /// <summary>
        /// Computes the expected gain of each spin, altought it is always 0 and so I decided not to use this.
        /// </summary>
        /// <param name="nSpins"></param>
        /// <param name="bet"></param>
        /// <returns></returns>
        private List<long> ComputeExpectedValue(int nSpins,int bet)
        {
            List<long> Eij = new List<long>();
            long ExpectedValuePerSpin = 0;
            for (int i=0;i< nSpins;i++)
            {
                foreach (List<int> x in CurrentBet)
                {
                        if (x.Count() == 18)
                        {
                            ExpectedValuePerSpin += (bet*(18/36))+(-bet*(18/36));
                        }
                        else if (x.Count() == 12)
                        {
                            ExpectedValuePerSpin += ( ((bet * 2)*(12/36))+(-bet*(24/36)) );
                        }
                        else if (x.Count() == 1)
                        {
                            ExpectedValuePerSpin += ( ( (bet * 31) * (1/36) )+(-bet*(35/36) )  );
                        }
                        else if (x.Count() == 2)
                        {
                            ExpectedValuePerSpin +=  ( (bet * 17)*(2/36) )+ (-bet*(34/36) );
                        }
                        else if (x.Count == 4)
                        {
                            ExpectedValuePerSpin += (bet * 8)*(4/36)+(-bet*(32/36));
                        }
                        else
                        {
                            ExpectedValuePerSpin += (bet * 11)*(3/36)+(-bet*(33/36) );
                        }
                }
                Eij.Add(ExpectedValuePerSpin);
                ExpectedValuePerSpin = 0;
            }
            return Eij;
        }
        /// <summary>
        /// This method computes what the gain is for each spin.
        /// </summary>
        /// <param name="Outcomes"></param>
        /// <param name="bet"></param>
        /// <returns></returns>
        private List<long> ComputeOutcomes(List<int> Outcomes,int bet)
        {
            List<long> Oij = new List<long>();
            long gain =0 ;
            foreach(int i in Outcomes)
            {
                foreach(List<int> x in CurrentBet)
                {
                    if (x.Contains(i))
                    {
                        if (x.Count() == 18)
                        {
                            gain += bet;
                        }
                        else if (x.Count() == 12)
                        {
                            gain += bet * 2;
                        }
                        else if (x.Count() == 1)
                        {
                            gain += bet * 31;
                        }
                        else if (x.Count() == 2)
                        {
                            gain += bet * 17;
                        }
                        else if (x.Count == 4)
                        {
                            gain += bet * 8;
                        }
                        else
                        {
                            gain += bet * 11;
                        }
                    }
                    else
                    {
                        gain += -1*bet;
                    }
                }
                Oij.Add(gain);
                gain = 0;
            }
            return Oij;
        }
        /// <summary>
        /// This method does hypothesis testing with Alpha:0.05.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="nSpins"></param>
        /// <param name="mean"></param>
        /// <returns></returns>
        private bool HypothesisTest(double s,int nSpins,double mean)
        {
            double zScore = mean / (s / Math.Sqrt(nSpins));
            if (zScore > 1.96 || zScore < -1.96)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        /// <summary>
        /// Event handler for the quit button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Quit_Click(object sender, RoutedEventArgs e)
        {
            Results_Screen.Visibility = Visibility.Collapsed;
        }
    }
}
